import numpy as np

# Read times and distances from the input file and store them in lists.
times, distances = [
    [int(n) for n in row.split(":")[1].split(" ") if n != ""]
    for row in open("data/06-input.txt", "r")
]


# Function to calculate the number of solutions to the quadratic equation.
def get_nb_sol(duration: int, distance: int):
    # Coefficients for the quadratic equation ax^2 + bx + c = 0.
    a = 1
    b = -1 * duration
    c = distance

    # Calculate the discriminant.
    discr = b**2 - 4 * a * c

    # Calculate the two solutions using the quadratic formula.
    x1 = np.floor((-1 * b - discr**0.5) / (2 * a)) + 1
    x2 = np.ceil((-1 * b + discr**0.5) / (2 * a)) - 1

    # Debugging print statement.
    # print(f"{duration} {distance} - {x1=} {x2=}")

    # Calculate the number of solutions within the range.
    nb_sol = x2 - x1 + 1

    return int(nb_sol)


# Calculate the product of the number of solutions for each time and distance pair.
result = 1
for t, d in zip(times, distances):
    result *= get_nb_sol(t, d)

# %%

# Read time and distance from the input file for the second calculation.
time2, distance2 = [
    int(row.split(":")[1].replace(" ", "")[:-1])
    for row in open("data/06-input.txt", "r")
]
# Calculate the result for the second part of the script.
result2 = get_nb_sol(time2, distance2)

# %%
