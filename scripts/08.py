from time import time

import numpy as np

file = "data/08-input.txt"
rows = [row for row in open(file, "r")]

nodes_id: dict[str, int] = {row.split(" = ")[0]: i for i, row in enumerate(rows[2:])}
nodes = [row.split(" = ")[0] for row in rows[2:]]

tree = {
    row.split(" = ")[0]: {
        "L": row.split(" = ")[1].split(", ")[0][-3:],
        "R": row.split(" = ")[1].split(", ")[1][:3],
    }
    for row in rows[2:]
}
lefts = np.empty(len(nodes_id), dtype=int)
rights = np.empty(len(nodes_id), dtype=int)

for key, val in tree.items():
    i = nodes_id[key]
    lefts[i] = nodes_id[val["L"]]
    rights[i] = nodes_id[val["R"]]

instructions = rows[0][:-1]

current_nodes_id = np.array(
    [nodes_id[node] for node in nodes_id.keys() if node[-1] == "A"]
)

z_id = [nodes_id[node] for node in nodes_id.keys() if node[-1] == "Z"]
z_bool = [node[-1] == "Z" for node in nodes]

i = 0
t0 = time()
while True:
    # "L" or "R"
    instruction = instructions[i % len(instructions)]

    i += 1
    if instruction == "L":
        current_nodes_id = lefts[current_nodes_id]
    else:
        current_nodes_id = rights[current_nodes_id]

    if i % 10_000_000 == 0:
        print(i)
        print(f"{time()-t0=:.1f}s")

    for node_id in current_nodes_id:
        if not z_bool[node_id]:
            break
    else:
        break

# After a few tries, the answer is higher than 1 billion.
# This loop is too slow



#%%
