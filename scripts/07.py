import numpy as np

# Setting whether jokers are included in the game.
with_jokers: bool = True

# Constants representing different hand types.
FIVE, FOUR, FULL, THREE, TWO_PAIR, ONE_PAIR, HIGH_CARD = 6, 5, 4, 3, 2, 1, 0

# Dictionary mapping card symbols to their values.
card_value: dict[str, int] = {
    "A": 14,
    "K": 13,
    "Q": 12,
    "J": 1 if with_jokers else 11,
    "T": 10,
    "9": 9,
    "8": 8,
    "7": 7,
    "6": 6,
    "5": 5,
    "4": 4,
    "3": 3,
    "2": 2,
}


# Function to calculate the value of a hand.
def value(s):
    val = 0
    # Convert the hand to a numerical value.
    for i, c in enumerate(s[::-1]):
        val += card_value[c] * (100**i)

    # Add the hand type to the value.
    val += hand_type(s) * 100 ** (len(s))
    return val


# Function to replace jokers with the most frequent letter
def replace_jokers(hand: str) -> str:
    count: dict[str, int] = count_letters(hand)
    count.pop("J", None)
    most_frequent_letter = max(count, key=count.get) if count else "A"

    return hand.replace("J", most_frequent_letter)


# Function to determine the type of a hand.
def hand_type(hand: str):
    if with_jokers:
        hand = replace_jokers(hand)

    count: dict[str, int] = count_letters(hand)
    # Logic to determine the hand type based on the counts of each card.
    if len(count) == 1:
        return FIVE
    if len(count) == 5:
        return HIGH_CARD
    if len(count) == 4:
        return ONE_PAIR

    if 4 in count.values():
        return FOUR
    if 3 in count.values():
        if 2 in count.values():
            return FULL
        return THREE

    return TWO_PAIR


# Function to count the occurrences of each card in a hand.
def count_letters(hand) -> dict[str, int]:
    count: dict = {}
    for c in hand:
        count[c] = count.get(c, 0) + 1
    return count


# Reading hands and bids from the file.
file = "data/07-input.txt"
hands = [row.split(" ")[0] for row in open(file, "r")]
hands_values = [value(hand) for hand in hands]
bids = [int(row.split(" ")[1][:-1]) for row in open(file, "r")]

# Sorting the hands based on their values.
ranks = np.argsort(hands_values)
bids_sorted = np.array(bids)[ranks]

# Calculating the final score.
res = 0
for i, x in enumerate(bids_sorted):
    res += (i + 1) * x

# Output the result.
print(res)

# %%
