# Dictionaries to store mappings and data.
maps: dict[str, dict] = {}
data: dict[str, list] = {}

# Variables to store the current source and destination.
source: str = ""
destination: str = ""
origin_seed_ids: list[int] = []

# Processing each row in the input file.
for row in open("data/05-input.txt", "r"):
    row: str
    # If the row contains seed data, process and store it.
    if "seeds:" in row:
        numbers = [int(n) for n in row.split(":")[1].split(" ") if n != ""]
        assert len(numbers) % 2 == 0
        data["seed"] = []
        for i in range(0, len(numbers), 2):
            data["seed"].append(
                {"range_begin": numbers[i], "range_end": numbers[i] + numbers[i + 1]}
            )

    # If the row defines a source-destination relationship, store it.
    elif "-to-" in row:
        source, destination = row.split(" ")[0].split("-to-")
        maps[source] = {}
        maps[source]["destination"] = destination
        maps[source]["ranges"] = []

    # If the row starts with a digit, it defines a range mapping.
    elif row[0].isdigit():
        dest_begin, source_begin, length = [int(n) for n in row.split(" ") if n != ""]
        maps[source]["ranges"].append(
            {
                "range_begin": source_begin,
                "range_end": source_begin + length,
                "offset": dest_begin - source_begin,
            }
        )

# %%


# Function to recursively get the destination category from a source category.
def recursive_get(source_categ):
    dest_categ = maps[source_categ]["destination"]
    data[dest_categ] = []

    # For each range from the source category, we will transform it according to the mappings.
    # They are themselves defined as mappings, hence it requires a lot of logic.
    for source_range in data[source_categ]:
        src_begin = source_range["range_begin"]
        src_end = source_range["range_end"]

        # Process each source range. On each loop, we update the src_begin.
        while src_begin < src_end:
            # Initialize variables for offset and next source beginning.
            offset = 0
            next_src_begin = src_end

            # Check if the current source begin is within any defined ranges.
            for map_range in maps[source_categ]["ranges"]:
                rge_begin = map_range["range_begin"]
                rge_end = map_range["range_end"]

                if rge_end < src_begin:
                    continue

                if rge_begin > src_begin:
                    next_src_begin = min(rge_begin, next_src_begin)
                    continue

                if rge_begin <= src_begin < rge_end:
                    offset = map_range["offset"]
                    next_src_begin = min(rge_end, next_src_begin)
                    continue

                assert True, "You should not be there..."

            # Append the calculated range to the destination category data.
            data[dest_categ].append(
                {
                    "range_begin": src_begin + offset,
                    "range_end": next_src_begin + offset,
                }
            )
            assert src_begin != next_src_begin, "Infinite loop"
            src_begin = next_src_begin

    # Base case: when destination category is 'location', return minimum range begin.
    if dest_categ == "location":
        return min([source_range["range_begin"] for source_range in data[dest_categ]])

    # Recursive case: call the function with the new destination category.
    return recursive_get(dest_categ)


# Calculating the result by starting the recursion with 'seed'.
result = recursive_get("seed")

# %%
