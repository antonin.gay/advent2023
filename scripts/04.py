import re
import numpy as np

# Initialize a variable to store the total sum.
total: int = 0
# Create a NumPy array to keep count of cards.
card_count = np.ones(194)

input_path = "data/04-input.txt"
# Process each row in the file.
for row in open(input_path, "r"):
    # Split the row into card ID and card data.
    card_id_str, card_data = row.split(":")
    # Extract numeric card ID using regular expression.
    card_id: int = int(re.sub("[^0-9^.]", "", card_id_str))

    # Split the card data into winning numbers and got numbers.
    winning_str, got_str = card_data.split("|")

    # Convert the winning and got numbers from string to integers.
    winning_nb: list[int] = [int(n) for n in winning_str.split(" ") if n != ""]
    got_nb: list[int] = [int(n) for n in got_str.split(" ") if n != ""]

    # Count the number of winning numbers in the got numbers.
    win_count = 0
    for n in got_nb:
        if n in winning_nb:
            win_count += 1

    # Update the total based on the win count.
    if win_count:
        total += 2 ** (win_count - 1)
        # Update card counts in the array based on win count and card ID.
        card_count[card_id : card_id + win_count] += card_count[card_id - 1]

# Calculate the second total as the sum of all card counts.
total2 = int(card_count.sum())

# %%
