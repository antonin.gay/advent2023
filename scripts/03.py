import numpy as np

file_path = "data/03-input.txt"

# Initializing numpy arrays to store information about symbols and stars.
symbol_neighbour = np.zeros((142, 142))
is_star = np.zeros((140, 140))

# Reading the file and populating the arrays.
for i, row in enumerate(open(file_path, "r")):
    for j, c in enumerate(row[:-1]):
        is_symbol = not (c.isdigit() or c == ".")
        # Setting neighbours to True, with offset due to edges trimmed later
        if is_symbol:
            symbol_neighbour[i : i + 3, j : j + 3] = True
        is_star[i, j] = c == "*"

# Trimming the edges of the symbol_neighbour array.
symbol_neighbour = symbol_neighbour[1:-1, 1:-1]

total = 0

# Variables for processing numbers and their validity in the text.
curr_number: int = 0
curr_valid: bool = False
curr_begin: int = 0
curr_end: int

# Arrays to store values and counts related to stars. They'll be trimmed later.
star_value = np.ones((142, 142))
star_nb_neighbours = np.zeros((142, 142))

# Processing each character in the file again.
for i, row in enumerate(open(file_path, "r")):
    for j, c in enumerate(row):
        if c.isdigit():
            if curr_number == 0:
                curr_begin = j
            curr_number = curr_number * 10 + int(c)
            curr_valid = curr_valid or symbol_neighbour[i, j]
        else:
            if curr_valid:
                curr_end = j
                total += curr_number
                star_value[i : i + 3, curr_begin : curr_end + 2] *= curr_number
                star_nb_neighbours[i : i + 3, curr_begin : curr_end + 2] += 1

            # Resetting the current number and its validity.
            curr_number = 0
            curr_valid = False

# Trimming the edges of the star-related arrays.
star_nb_neighbours = star_nb_neighbours[1:-1, 1:-1]
star_value = star_value[1:-1, 1:-1]

# Calculating a secondary total based on certain conditions involving stars.
total2 = np.sum(is_star * (star_nb_neighbours == 2) * star_value, dtype=int)

# %%
