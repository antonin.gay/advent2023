total = 0  # Initialize a variable to store the total sum.
numbers = []  # Create an empty list to store the processed numbers.
digits = [
    "_",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
]  # List of digit names for replacement.

# Read each row from the file '01-input1.txt'.
for row in open("data/01-input1.txt", "r"):
    # Replace each word (digit name) in the row with a format 'digitNamedigitNumberdigitName'.
    for i, digit in enumerate(digits):
        row = row.replace(digit, digit + str(i) + digit)

    # Initialize variables to hold the first and last digit found in the row.
    first_digit, last_digit = 0, 0

    # Find the first numeric digit in the row.
    for c in row:
        try:
            first_digit = int(c)
            break
        except ValueError:
            pass  # Continue if the character is not a digit.

    # Find the last numeric digit in the row, searching from the end.
    for c in row[::-1]:
        try:
            last_digit = int(c)
            break
        except ValueError:
            pass  # Continue if the character is not a digit.

    # Compute a number from the first and last digits and add it to the list and the total.
    numbers.append(10 * first_digit + last_digit)
    total += 10 * first_digit + last_digit

# %%
