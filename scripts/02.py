import re

# %% Functions


def is_draw_possible(_game_data: str):
    """Function to check if a draw is possible based on the game data."""
    # Maximum dices available for each color.
    max_dices: dict[str, int] = {"red": 12, "green": 13, "blue": 14}

    # Iterating through each draw in the game data.
    for draw in _game_data.split(";"):
        # Splitting each draw into its colored components.
        for color_draw in draw.split(","):
            _, count, color = color_draw.replace("\n", "").split(" ")

            # Checking if the count exceeds the maximum dice available for the color.
            if int(count) > max_dices[color]:
                return False  # Draw is not possible if the count exceeds the max.

    return True  # Draw is possible if all counts are within the max.


def draw_power(_game_data: str):
    """Function to calculate the draw power based on the game data."""
    # Initializing minimum dices required for each color.
    min_dices: dict[str, int] = {"red": 0, "green": 0, "blue": 0}

    # Iterating through each draw in the game data.
    for draw in _game_data.split(";"):
        # Splitting each draw into its colored components.
        for color_draw in draw.split(","):
            _, count, color = color_draw.replace("\n", "").split(" ")

            # Updating the min dices if the current count is higher.
            if int(count) > min_dices[color]:
                min_dices[color] = int(count)

    # Calculating and returning the draw power.
    return min_dices["red"] * min_dices["green"] * min_dices["blue"]


# %% Run script

total1: int = 0  # Variable to accumulate total for the first condition.
total2: int = 0  # Variable to accumulate total draw power.

# Reading and processing each row in the input file.
for row in open("data/02-input.txt", "r"):
    game_id, game_data = row.split(":")
    _id = int(re.sub("[^0-9^.]", "", game_id))

    # Adding the id to total1 if the draw is possible.
    if is_draw_possible(game_data):
        total1 += _id

    # Adding the draw power to total2.
    total2 += draw_power(game_data)

# %%
